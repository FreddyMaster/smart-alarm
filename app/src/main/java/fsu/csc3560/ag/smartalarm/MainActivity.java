package fsu.csc3560.ag.smartalarm;

import static fsu.csc3560.ag.smartalarm.R.*;
import static fsu.csc3560.ag.smartalarm.R.id.*;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main);

        // Find the BottomNavigationView by its ID
        BottomNavigationView mBottomNavigationView = findViewById(bottomNavigationView);

        mBottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case alarm:
                    // Launch the Alarm Fragment
                    getSupportFragmentManager().beginTransaction()
                            .replace(fragment_container, new AlarmFragment())
                            .commit();
                    return true;
                case timer:
                    // Handle timer action
                    // Launch the TimerFragment
                    getSupportFragmentManager().beginTransaction()
                            .replace(fragment_container, new TimerFragment())
                            .commit();
                    return true;
                case settings:
                    // Handle settings action
                    getSupportFragmentManager().beginTransaction()
                            .replace(fragment_container, new SettingsFragment())
                            .commit();
                    return true;

                default:
                    // Launch the alarm Fragment
                    getSupportFragmentManager().beginTransaction()
                            .replace(fragment_container, new AlarmFragment())
                            .commit();
            }
            return false;
        });

    }
}
