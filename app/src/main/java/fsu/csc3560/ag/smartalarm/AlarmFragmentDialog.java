package fsu.csc3560.ag.smartalarm;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.preference.PreferenceManager;

public class AlarmFragmentDialog extends DialogFragment {

    private Ringtone ringtone;
    private Button stopButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the dialog view
        View view = inflater.inflate(R.layout.dialog_alarm, container, false);

        // Get the ringtone from the arguments
        Bundle arguments = getArguments();
        if (arguments != null) {
            Uri ringtoneUri = arguments.getParcelable("ringtoneUri");
            ringtone = RingtoneManager.getRingtone(requireActivity(), ringtoneUri);
        }

        // Set up the message text
        TextView messageTextView = view.findViewById(R.id.messageTextView);
        messageTextView.setText("Time's up!");

        // Set up the stop button click listener
        stopButton = view.findViewById(R.id.buttonStop);
        stopButton.setOnClickListener(v -> {
            // Stop the ringtone
            ringtone.stop();

            // Dismiss the dialog
            dismiss();
        });

        // Return the inflated view
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        // Show the dialog fullscreen
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                window.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            }
        }

        // Start playing the ringtone
        if (ringtone != null) {
            ringtone.play();
        }
    }

    @Override
    public void onDestroyView() {
        // Remove the stop button click listener to prevent it from being called multiple times
        if (stopButton != null) {
            stopButton.setOnClickListener(null);
        }

        super.onDestroyView();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // Create a custom dialog
        Dialog dialog = new Dialog(requireContext(), R.style.DialogFullScreen);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }
}
