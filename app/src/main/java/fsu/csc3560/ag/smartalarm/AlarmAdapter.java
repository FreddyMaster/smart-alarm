package fsu.csc3560.ag.smartalarm;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AlarmAdapter extends BaseAdapter {

    private final Context mContext;
    private final ArrayList<Alarm> mAlarmList;
    private List<String> mSelectedDays = new ArrayList<>();

    public AlarmAdapter(Context context) {
        mContext = context;
        mAlarmList = new ArrayList<>();
    }

    public void addAlarm(Alarm alarm) {
        mAlarmList.add(alarm);
        notifyDataSetChanged();
    }

    public void removeAlarm(Alarm alarm) {
        mAlarmList.remove(alarm);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mAlarmList.size();
    }

    @Override
    public Object getItem(int position) {
        return mAlarmList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_alarm, parent, false);
        }

        Alarm alarm = mAlarmList.get(position);


        TextView timeTextView = convertView.findViewById(R.id.alarm_time_text);
        CheckBox sundayCheckBox = convertView.findViewById(R.id.sunday_checkbox);
        CheckBox mondayCheckBox = convertView.findViewById(R.id.monday_checkbox);
        CheckBox tuesdayCheckBox = convertView.findViewById(R.id.tuesday_checkbox);
        CheckBox wednesdayCheckBox = convertView.findViewById(R.id.wednesday_checkbox);
        CheckBox thursdayCheckBox = convertView.findViewById(R.id.thursday_checkbox);
        CheckBox fridayCheckBox = convertView.findViewById(R.id.friday_checkbox);
        CheckBox saturdayCheckBox = convertView.findViewById(R.id.saturday_checkbox);

        View finalConvertView = convertView;
        sundayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> updateSelectedDays(isChecked, "Sun", finalConvertView));

        mondayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> updateSelectedDays(isChecked, "Mon", finalConvertView));

        tuesdayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> updateSelectedDays(isChecked, "Tue", finalConvertView));

        wednesdayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> updateSelectedDays(isChecked, "Wed", finalConvertView));

        thursdayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> updateSelectedDays(isChecked, "Thu", finalConvertView));

        fridayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> updateSelectedDays(isChecked, "Fri",finalConvertView));

        saturdayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> updateSelectedDays(isChecked, "Sat", finalConvertView));

        timeTextView.setText(alarm.getTimeString());

        SwitchMaterial enabledSwitch = (SwitchMaterial) convertView.findViewById(R.id.alarm_switch);
        enabledSwitch.setChecked(alarm.isEnabled());
        enabledSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
               timeTextView.setTypeface(null, Typeface.BOLD);
            } else {
                timeTextView.setTypeface(null, Typeface.NORMAL);
            }
            alarm.setEnabled(isChecked);
        });

        ImageButton deleteButton = convertView.findViewById(R.id.buttonDelete);
        deleteButton.setOnClickListener(v -> removeAlarm(alarm));

        ImageView expandImageView = convertView.findViewById(R.id.buttonExpand);
        LinearLayout detailsLayout = convertView.findViewById(R.id.alarm_expanded_layout);

        // Set visibility of details layout and expand image based on alarm's expanded state
        if (alarm.isExpanded()) {
            detailsLayout.setVisibility(View.VISIBLE);
            expandImageView.setImageResource(R.drawable.ic_collapse_foreground);
        } else {
            detailsLayout.setVisibility(View.GONE);
            expandImageView.setImageResource(R.drawable.ic_expand_foreground);
        }

        // Toggle expanded state of alarm and update visibility of details layout and expand image
        expandImageView.setOnClickListener(v -> {
            alarm.setExpanded(!alarm.isExpanded());
            if (alarm.isExpanded()) {
                detailsLayout.setVisibility(View.VISIBLE);
                expandImageView.setImageResource(R.drawable.ic_collapse_foreground);
            } else {
                detailsLayout.setVisibility(View.GONE);
                expandImageView.setImageResource(R.drawable.ic_expand_foreground    );
            }
        });

        return convertView;
    }

    private void updateSelectedDays(boolean isChecked, String dayOfWeek, View view) {
        if (isChecked) {
            mSelectedDays.add(dayOfWeek);
        } else {
            mSelectedDays.remove(dayOfWeek);
        }
        updateDaysText(view);
    }

    private void updateDaysText(View view) {
        String daysSelected = "";
        for (int i = 0; i < mSelectedDays.size(); i++) {
            daysSelected += mSelectedDays.get(i);
            if (i < mSelectedDays.size() - 1) {
                daysSelected += ", ";
            }
        }
        TextView daysTextView = view.findViewById(R.id.daysSelected);
        daysTextView.setText(daysSelected);
    }
}
