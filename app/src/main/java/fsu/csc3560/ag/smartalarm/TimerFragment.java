package fsu.csc3560.ag.smartalarm;

import static fsu.csc3560.ag.smartalarm.R.*;

import android.app.Dialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Locale;

public class TimerFragment extends Fragment implements View.OnClickListener {

    private TextView editTextMinutes;
    private TextView editTextSeconds;
    private TextView editTextHours;

    private Button buttonStart;
    private Button buttonStop;
    private  Button buttonReset;

    private CountDownTimer countDownTimer;
    private boolean timerRunning;
    private long timeLeftInMillis;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(layout.fragment_timer, container, false);

        editTextMinutes = rootView.findViewById(id.tensEditText);
        editTextSeconds = rootView.findViewById(id.onesEditText);
        editTextHours = rootView.findViewById(id.hundredsEditText);
        buttonStart = rootView.findViewById(id.startButton);
        buttonStop = rootView.findViewById(id.stopButton);
        buttonReset = rootView.findViewById(id.resetButton);

        // Dial pad buttons
        Button buttonOne = rootView.findViewById(id.buttonOne);
        Button buttonTwo = rootView.findViewById(id.buttonTwo);
        Button buttonThree = rootView.findViewById(id.buttonThree);
        Button buttonFour = rootView.findViewById(id.buttonFour);
        Button buttonFive = rootView.findViewById(id.buttonFive);
        Button buttonSix = rootView.findViewById(id.buttonSix);
        Button buttonSeven = rootView.findViewById(id.buttonSeven);
        Button buttonEight = rootView.findViewById(id.buttonEight);
        Button buttonNine = rootView.findViewById(id.buttonNine);
        Button buttonZero = rootView.findViewById(id.buttonZero);
        Button buttonZeroZero = rootView.findViewById(id.buttonZeroZero);


        buttonStart.setOnClickListener(this);
        buttonStop.setOnClickListener(this);
        buttonReset.setOnClickListener(this);

        buttonOne.setOnClickListener(this);
        buttonTwo.setOnClickListener(this);
        buttonThree.setOnClickListener(this);
        buttonFour.setOnClickListener(this);
        buttonFive.setOnClickListener(this);
        buttonSix.setOnClickListener(this);
        buttonSeven.setOnClickListener(this);
        buttonEight.setOnClickListener(this);
        buttonNine.setOnClickListener(this);
        buttonZero.setOnClickListener(this);

        // Set up the TextWatchers for the EditText fields
        editTextMinutes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 2) {
                    editTextSeconds.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTextSeconds.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 2) {
                    editTextSeconds.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        buttonStart.setOnClickListener(this);
        buttonStop.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(@NonNull View view) {
        switch (view.getId()) {
            case id.startButton:
                startTimer();
                break;
            case id.stopButton:
                stopTimer();
                break;
            case id.resetButton:
                resetTimer();
                break;
            case id.buttonZero:
                // Handle button 0 press
                appendNumber(0);
                break;
            case id.buttonZeroZero:
                // Handle button 00 press
                appendNumber(0);
                appendNumber(0);
                break;
            case id.buttonOne:
                // Handle button 1 press
                appendNumber(1);
                break;
            case id.buttonTwo:
                // Handle button 2 press
                appendNumber(2);
                break;
            case id.buttonThree:
                // Handle button 3 press
                appendNumber(3);
                break;
            case id.buttonFour:
                // Handle button 4 press
                appendNumber(4);
                break;
            case id.buttonFive:
                // Handle button 5 press
                appendNumber(5);
                break;
            case id.buttonSix:
                // Handle button 6 press
                appendNumber(6);
                break;
            case id.buttonSeven:
                // Handle button 7 press
                appendNumber(7);
                break;
            case id.buttonEight:
                // Handle button 8 press
                appendNumber(8);
                break;
            case id.buttonNine:
                // Handle button 9 press
                appendNumber(9);
                break;
            case id.buttonDel:
                // Handle button Del press
                deleteNumber();
                break;
        }
    }

    // Initialize counter variable
    int counter = 0;

    // Initialize StringBuilder for each TextView
    StringBuilder onesBuilder = new StringBuilder();
    StringBuilder minutesBuilder = new StringBuilder();
    StringBuilder hoursBuilder = new StringBuilder();
    private void appendNumber(int number) {
        if (onesBuilder.length() < 2) {
            if (onesBuilder.length() == 1 && onesBuilder.charAt(0) == '0') {
                onesBuilder.deleteCharAt(0);
            }
            onesBuilder.append(number);
            editTextSeconds.setText(onesBuilder.toString());
            if (onesBuilder.length() == 1) {
                editTextSeconds.setText("0" + onesBuilder.toString());
            }
        } else if (minutesBuilder.length() < 2) {
            if (minutesBuilder.length() == 1 && minutesBuilder.charAt(0) == '0') {
                minutesBuilder.deleteCharAt(0);
            }
            minutesBuilder.append(number);
            editTextMinutes.setText(minutesBuilder.toString());
            if (minutesBuilder.length() == 1) {
                editTextMinutes.setText("0" + minutesBuilder.toString());
            }
        } else if (hoursBuilder.length() < 2) {
            if (hoursBuilder.length() == 1 && hoursBuilder.charAt(0) == '0') {
                hoursBuilder.deleteCharAt(0);
            }
            hoursBuilder.append(number);
            editTextHours.setText(hoursBuilder.toString());
            if (hoursBuilder.length() == 1) {
                editTextHours.setText("0" + hoursBuilder.toString());
            }
        }
    }

    public void deleteNumber() {
        if (onesBuilder.length() > 0) {
            onesBuilder.deleteCharAt(onesBuilder.length() - 1);
        } else if (minutesBuilder.length() > 0) {
            minutesBuilder.deleteCharAt(minutesBuilder.length() - 1);
        } else if (hoursBuilder.length() > 0) {
            hoursBuilder.deleteCharAt(hoursBuilder.length() - 1);
        }
    }

    private void startTimer() {
        String inputMinutes = editTextMinutes.getText().toString().replaceAll("[^\\d.]", ""); // remove non-numeric characters
        String inputSeconds = editTextSeconds.getText().toString().replaceAll("[^\\d.]", ""); // remove non-numeric characters
        String inputHours = editTextHours.getText().toString().replaceAll("[^\\d.]", ""); // remove non-numeric characters

        int minutes = inputMinutes.isEmpty() ? 0 : Integer.parseInt(inputMinutes);
        int seconds = inputSeconds.isEmpty() ? 0 : Integer.parseInt(inputSeconds);
        int hours = inputHours.isEmpty() ? 0 : Integer.parseInt(inputHours);
        long hoursInMillis = hours * 60L * 60L * 1000L; // convert hours to milliseconds

        if (minutes == 0 && seconds == 0) {
            Toast.makeText(getContext(), "Please enter a time.", Toast.LENGTH_SHORT).show();
            return;
        }

        timeLeftInMillis = (hoursInMillis + (minutes * 60L + seconds) * 1000L);
        startCountDownTimer();
    }
    private void resetTimer() {
        // Stop the timer and set the timerRunning flag to false
        stopTimer();
        timerRunning = false;

        // Reset the timeLeftInMillis to the initial value
        timeLeftInMillis = 0;

        // Reset the hours, minutes, and seconds TextViews to "00"
        editTextHours.setText("00");
        editTextMinutes.setText("00");
        editTextSeconds.setText("00");

        // Reset the StringBuilder objects to empty strings
        onesBuilder = new StringBuilder();
        minutesBuilder = new StringBuilder();
        hoursBuilder = new StringBuilder();
    }


    public void startCountDownTimer() {
        countDownTimer = new CountDownTimer(timeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                timerRunning = false;
                buttonStart.setEnabled(true);
                buttonReset.setEnabled(true);
                buttonStop.setEnabled(false);
                showTimerDialog();
            }
        }.start();

        timerRunning = true;
        buttonStart.setEnabled(false);
        buttonStop.setEnabled(true);
    }

    private void stopTimer() {
        if (timerRunning) {
            countDownTimer.cancel();
            timerRunning = false;
            buttonStart.setEnabled(true);
            buttonStop.setEnabled(false);
        }
    }

    public void updateCountDownText() {
        int hours = (int) (timeLeftInMillis / 1000) / 3600;
        int minutes = (int) (timeLeftInMillis / 1000) / 60;
        int seconds = (int) (timeLeftInMillis / 1000) % 60;

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        editTextHours.setText(String.format(Locale.getDefault(), "%02d", hours));
        editTextMinutes.setText(String.format(Locale.getDefault(), "%02d", minutes));
        editTextSeconds.setText(String.format(Locale.getDefault(), "%02d", seconds));
    }

    public void showTimerDialog() {
        // Create a full-screen dialog
        Dialog dialog = new Dialog(requireContext(), android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        dialog.setContentView(R.layout.dialog_alarm);
        MediaPlayer mediaPlayer = MediaPlayer.create(requireContext(), Settings.System.DEFAULT_NOTIFICATION_URI);


        // Set up the dialog views
        TextView messageTextView = dialog.findViewById(R.id.messageTextView);
        Button stopButton = dialog.findViewById(R.id.buttonStop);

        // Set the message text
        messageTextView.setText("Time's up!");

        // Set up the stop button click listener
        stopButton.setOnClickListener(v -> {
            // Stop the sound
            mediaPlayer.stop();

            // Release the MediaPlayer object
            mediaPlayer.release();

            // Dismiss the dialog
            dialog.dismiss();
        });


        // Show the dialog
        dialog.show();

        // Play the timer sound
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

}
