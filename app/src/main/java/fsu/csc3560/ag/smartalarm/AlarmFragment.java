package fsu.csc3560.ag.smartalarm;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class AlarmFragment extends Fragment {

    public static final String ALARM_LIST_KEY = "alarm_list_key";
    private AlarmAdapter mAlarmAdapter;
    int alarmId = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_alarm, container, false);

        // Set up the ListView and adapter
        ListView mAlarmsList = (ListView) rootView.findViewById(R.id.alarms_list);
        mAlarmAdapter = new AlarmAdapter(getActivity());
        mAlarmsList.setAdapter(mAlarmAdapter);

        // Set up the add alarm button
        Button addAlarmButton = (Button) rootView.findViewById(R.id.add_alarm_button);
        addAlarmButton.setOnClickListener(v -> showAddAlarmDialog());

        // Load saved alarms from SharedPreferences and display them
        loadSavedAlarms();
        displayAlarms();

        return rootView;
    }

    private void loadSavedAlarms() {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        String json = sharedPreferences.getString(ALARM_LIST_KEY, null);
        Type type = new TypeToken<ArrayList<Alarm>>() {
        }.getType();
        ArrayList<Alarm> alarmList = gson.fromJson(json, type);
        if (alarmList != null) {
            for (Alarm alarm : alarmList) {
                mAlarmAdapter.addAlarm(alarm);
            }
        }
    }

    private void setAlarm(Alarm alarm) {
        AlarmManager alarmManager = (AlarmManager) requireActivity().getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(getContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), alarm.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, alarm.getHour());
        calendar.set(Calendar.MINUTE, alarm.getMinute());
        calendar.set(Calendar.SECOND, 0);

        if (calendar.before(Calendar.getInstance())) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }


    private void addAlarm(Alarm alarm) {
        mAlarmAdapter.addAlarm(alarm);

        Gson gson = new Gson();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireActivity().getApplicationContext());
        String json = sharedPreferences.getString(ALARM_LIST_KEY, null);
        Type type = new TypeToken<ArrayList<Alarm>>() {
        }.getType();
        ArrayList<Alarm> alarmList = gson.fromJson(json, type);
        if (alarmList == null) {
            alarmList = new ArrayList<>();
        }
        alarmList.add(alarm);
        json = gson.toJson(alarmList);
        sharedPreferences.edit().putString(ALARM_LIST_KEY, json).apply();
    }


    private void showAddAlarmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View dialogView = inflater.inflate(R.layout.dialog_add_alarm, null);
        TimePicker timePicker = dialogView.findViewById(R.id.alarm_time_picker);

        MaterialButton cancelButton = dialogView.findViewById(R.id.cancel_button);
        MaterialButton saveButton = dialogView.findViewById(R.id.add_alarm_button);

        builder.setView(dialogView);

        AlertDialog dialog = builder.create();

        cancelButton.setOnClickListener(view -> dialog.dismiss());

        saveButton.setOnClickListener(view -> {
            int hour, minute;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                hour = timePicker.getHour();
                minute = timePicker.getMinute();
            } else {
                hour = timePicker.getCurrentHour();
                minute = timePicker.getCurrentMinute();
            }

            Alarm alarm = new Alarm(hour, minute, true, getContext(), alarmId++);
            addAlarm(alarm);
            setAlarm(alarm);
            dialog.dismiss();
            Toast.makeText(getContext(), "Added Alarm!.", Toast.LENGTH_SHORT).show();
        });

        dialog.show();
    }

    private void displayAlarms() {
        mAlarmAdapter.notifyDataSetChanged();
    }

    private void deleteAlarm(Alarm alarm) {
        // Get the list of alarms from the SharedPreferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireActivity().getApplicationContext());
        Gson gson = new Gson();
        String json = sharedPreferences.getString(ALARM_LIST_KEY, null);
        Type type = new TypeToken<List<Alarm>>() {}.getType();
        List<Alarm> alarmList = gson.fromJson(json, type);

        // Remove the alarm from the list of alarms
        if (alarmList != null) {
            alarmList.remove(alarm);
        }

        // Save the updated list of alarms to the SharedPreferences
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ALARM_LIST_KEY, gson.toJson(alarmList));
        editor.apply();

        // Update the alarms list on the UI
        displayAlarms();
    }

}
