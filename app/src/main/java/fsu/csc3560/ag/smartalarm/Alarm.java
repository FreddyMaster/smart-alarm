package fsu.csc3560.ag.smartalarm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class Alarm {
    private int hour;
    private int minute;
    private int id;
    private boolean isEnabled;
    private boolean isExpanded;
    private String label;
    private final PendingIntent mPendingIntent;
    private static final String ACTION_ALARM = "fsu.csc3560.ag.smartalarm.ACTION_ALARM";
    private static final String EXTRA_ALARM_ID = "fsu.csc3560.ag.smartalarm.EXTRA_ALARM_ID";

    public PendingIntent getPendingIntent() {
        return mPendingIntent;
    }

    private PendingIntent createPendingIntent(Context context, int alarmId) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction(ACTION_ALARM);
        intent.putExtra(EXTRA_ALARM_ID, alarmId);

        return PendingIntent.getBroadcast(context, alarmId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public Alarm(int hour, int minute, boolean isEnabled, Context context, int alarmId) {
        this.hour = hour;
        this.minute = minute;
        this.isEnabled = isEnabled;
        mPendingIntent = createPendingIntent(context, alarmId);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public void setExpanded(boolean enabled) {
        isExpanded = enabled;
    }

    public String getTimeString() {
        String hourString = hour < 10 ? "0" + hour : String.valueOf(hour);
        String minuteString = minute < 10 ? "0" + minute : String.valueOf(minute);
        return hourString + ":" + minuteString;
    }

    public int getId() {
        return id;
    }
}
