package fsu.csc3560.ag.smartalarm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.os.Bundle;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceClickListener {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings, rootKey);

        Preference alarmSoundPreference = findPreference("alarm_sound");
        Objects.requireNonNull(alarmSoundPreference).setOnPreferenceClickListener(this);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.getKey().equals("alarm_sound")) {
            RingtoneManager manager = new RingtoneManager(getContext());
            manager.setType(RingtoneManager.TYPE_ALARM);
            Cursor cursor = manager.getCursor();

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

            final List<String> ringtoneTitles = new ArrayList<>();
            final List<String> ringtoneUris = new ArrayList<>();

            while (cursor.moveToNext()) {
                String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
                String uri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX) + "/" + cursor.getString(RingtoneManager.ID_COLUMN_INDEX);
                ringtoneTitles.add(title);
                ringtoneUris.add(uri);
            }
            cursor.close();

            CharSequence[] items = ringtoneTitles.toArray(new CharSequence[0]);
            builder.setItems(items, (dialog, item) -> {
                String selectedUri = ringtoneUris.get(item);
                preference.setSummary(selectedUri);
                Objects.requireNonNull(preference.getSharedPreferences()).edit().putString(preference.getKey(), selectedUri).apply();
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }
        return false;
    }

}



