package fsu.csc3560.ag.smartalarm;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

public class AddTimerFragment extends Fragment {

    private EditText editTextTime;

    public AddTimerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.add_timer, container, false);

        Button button1 = view.findViewById(R.id.button1);
        Button button2 = view.findViewById(R.id.button2);
        Button button3 = view.findViewById(R.id.button3);
        Button button4 = view.findViewById(R.id.button4);
        Button button5 = view.findViewById(R.id.button5);
        Button button6 = view.findViewById(R.id.button6);
        Button button7 = view.findViewById(R.id.button7);
        Button button8 = view.findViewById(R.id.button8);
        Button button9 = view.findViewById(R.id.button9);
        editTextTime = view.findViewById(R.id.editTextTime);

        // Set listeners for the number buttons
        button1.setOnClickListener(view19 -> editTextTime.append("1"));

        button2.setOnClickListener(view18 -> editTextTime.append("2"));

        button3.setOnClickListener(view17 -> editTextTime.append("3"));

        button4.setOnClickListener(view16 -> editTextTime.append("4"));

        button5.setOnClickListener(view15 -> editTextTime.append("5"));

        button6.setOnClickListener(view14 -> editTextTime.append("6"));

        button7.setOnClickListener(view13 -> editTextTime.append("7"));

        button8.setOnClickListener(view12 -> editTextTime.append("8"));

        button9.setOnClickListener(view1 -> editTextTime.append("9"));

        return view;
    }
}
